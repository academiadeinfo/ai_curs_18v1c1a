#include <iostream>

using namespace std;

int main()
{
    string pahar1, pahar2;

    cin >> pahar1 >> pahar2;

    string paharAux;

    paharAux = pahar1;
    pahar1 = pahar2;
    pahar2 = paharAux;

    cout << pahar1 << " " << pahar2 << endl;

    return 0;
}
