#include <iostream>

using namespace std;

int main()
{
    int nr;
    int contor = 0; /// câte numere am citit

    while(nr != 14 && contor < 15) ///cât timp numărul introdus este diferit de 14
    {
        cout << "Introduceti un numar diferit de 14: ";
        cin >> nr;
        contor = contor + 1; ///cresc contorul cu 1
    }
    if(contor == 15) ///dacă contorul a ajuns la 15
        cout << endl << "Wow! cata rabdare ai!" << endl;
    else ///altfel îseamnă că am ieșit din while pentru că a fost introdus 14
        cout << endl << "Hey! Am spus diferit de 14." << endl;

    return 0;
}
