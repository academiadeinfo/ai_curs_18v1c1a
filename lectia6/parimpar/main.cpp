#include <iostream>

using namespace std;

///Scrieți un program care citește un număr natural n și verifică
/// dacă este par sau impar
/// un număr este par dacă restul împărțirii la 2 este 0 (n % 2 == 0)

/**
 (*) Scrieți un program care citește 4 numere și spune câte dintre ele erau pare
    ex: 2, 6, 23, 41  va afișa 2
*/

int main()
{
    int n;
    cout << "n = ";
    cin >> n;
    cout << endl;

    if(n % 2 == 0)
        cout << "par";
    else
        cout << "impar";

    cout << endl;

    return 0;
}
