#include <iostream>

using namespace std;
/**
Scrieți un program care citește câte 2 numere și afișează după fiecare pereche maximul dintre ele.

b) Se va adăuga o condiție de oprire:
programul va continua cat timp cel puțin unul din numerele citite este diferit de 0
*/

int main()
{
    int a, b;

    while(a != 0 or b != 0)
    {
        cout << "Introduceti doua numere: ";
        cin >> a >> b;
        if(a > b)
            cout << "Maximul este: " << a;
        else
            cout << "Maximul este: " << b;
    }

    return 0;
}
