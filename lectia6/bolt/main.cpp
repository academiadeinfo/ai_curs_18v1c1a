#include <iostream>

using namespace std;

/**
Elif și Elsa joacă Bolț. Ei spun numere consecutive pe rând, începând de la 1. Dacă un număr este divizibil cu 7 sau are ultima cifră 7 în locul numărului ei trebuie să spună "Bolț!" și să treacă mai departe. Cine greșește pierde. Câteodată ei ajung la numere destul de mari și Elif nu mai reușește să calculeze în minte, așa că vă roagă să faceți un program care să îl ajute să continue jocul.

    Programul va citi ultimul număr pe care l-a zis Elsa și afișează ce trebuie să spună Elif.
    Dacă Elsa a greșit Elif o să spună "Am castigat"
    Altfel, Elif o să spună următorul număr sau, dacă e cazul, "Bolț".

*/

int main()
{
    int elsa; ///ultimul număr spus de Elsa
    int elif; ///numărul următor

    cout << "Elsa: ";
    cin >> elsa;

    if(elsa % 7 == 0 or elsa % 10 == 7)///Dacă elsa a gresit
        cout << "Elif: Am castigat!" << endl; ///afișez "Am câștigat"
    else ///Altfel:
    {
        elif = elsa + 1;///calculez următorul număr
        if(elif % 7 == 0 or elif % 10 == 7)/// Dacă următorul număr respectă regula:
            cout << "Elif: BOLT!" << endl;///afișez BOLȚ,
        else
            cout << "Elif: " << elif << endl;/// Altfel afișez numărul
    }

    return 0;
}
