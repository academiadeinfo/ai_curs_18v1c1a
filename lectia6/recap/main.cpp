#include <iostream>

using namespace std;

int main()
{
    int x, y;

    cout << "Cate prajituri a mancat Elif? ";
    cin >> x;
    cout << "Cate ptajituri a mancat Elsa? ";
    cin >> y;

    if(x > y)
        cout << "Elif a mancat mai multe" << endl;
    else if(y > x)
        cout << "Elsa a mancat mai multe" << endl;
    else
        cout << "Au mancat la fel de multe" << endl;

    return 0;
}
