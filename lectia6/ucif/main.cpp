#include <iostream>

using namespace std;

int main()
{
    ///Se citește un număr natural n
    ///Să se verifice dacă ultima cifră a lui n este mai mare sau egală cu 5
    /// Ultima cifră a unui număr este restul împărțirii lui la 10 ( n % 10 )

    int n;

    cin >> n;

    if( n % 10 >= 5)
        cout << "Ucif >=  5" << endl;
    else
        cout << "Ucif < 5" << endl;





    return 0;
}
