#include <iostream>

using namespace std;

int main()
{
    ///citim un număr, un semn și al doilea număr
    int a, b;
    char semn;

    cin >> a >> semn >> b;

    ///afișăm rezultatul
    if(semn == '+')
        cout << " = " << a + b;
    else if(semn == '-')
        cout << " = " << a - b;
    else if(semn=='*')
        cout<<"="<<a*b;
    else if(semn=='/')
        cout<<"="<<a/b;
    else if(semn=='%')
        cout<<"="<<a%b;

    return 0;
}
