#include <iostream>

using namespace std;

int main()
{
    string nume = "Whiley";
    string c1 = "alb", c2 = "caine", c3 = "jucaus", c4 = "pufos";
    string intreb;

    cout << nume <<" este ..." << endl;
    cin >> intreb;

    if(intreb == c1 || intreb == c2 || intreb == c3 || intreb == c4)
        cout << "Intradevar, " << nume << " este " << intreb << ". " << endl;
    else
        cout << "Nu, " << nume << " nu este " << intreb << ". " << endl;


    return 0;
}
