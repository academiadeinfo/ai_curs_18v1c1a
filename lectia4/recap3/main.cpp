#include <iostream>

using namespace std;

/**
Se citesc 2 cuvinte, să se afișeze
dacă acesta sunt sau nu sunt identice
*/

int main()
{
    string cuv1, cuv2; ///declar două variabile de tip string

    cin >> cuv1 >> cuv2; /// citesc cele 2 cuvinte

    if (cuv1 == cuv2)  /// dacă cuv1 este egal cu cuv2
    {
        cout << "Cuvintele sunt identice" << endl; ///atunci afișez mesajul
    }
    else ///altfel
    {
        cout << "Cuvintele nu sunt identice" << endl; ///afișez celelalt mesaj
    }

    return 0;
}
