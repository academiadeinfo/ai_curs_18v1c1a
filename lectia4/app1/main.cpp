#include <iostream>

using namespace std;

int main()
{
    ///declarăm variabile
    string nume, prenume;
    double nota1, nota2, nota3;
    double media;

    ///citire
    cout << "Numele si prenumele elevului: ";
    cin >> nume >> prenume;

    cout << "Introduceti notele: ";
    cin >> nota1 >> nota2 >> nota3;

    ///rezolvare și afișare
    media = (nota1 + nota2 +nota3) / 3;
    cout << "Media elevului " << nume << " " << prenume << " este " << media << endl;

    if(media >= 5)
        cout << "A trecut clasa! Bravo!" << endl; /// o singură instrucțiune, acoladele pot lipsi
    else
    {
        cout << "Il revedem la toamna " << endl;
    }



    return 0;
}
