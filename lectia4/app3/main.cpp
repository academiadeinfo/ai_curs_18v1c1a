#include <iostream>

using namespace std;

/**
Scrieți un program care citește Numele și vârsta a două persoane
Și afișează numele persoanei cu vârsta mai mare
*/

int main()
{
    string nume1, nume2;
    int varsta1, varsta2;

    cout << "Introduceti numele si varsta primei persoane: " << endl;
    cin >> nume1 >> varsta1;
    cout << "Introduceti numele si varsta celei de a 2-a persoane: " << endl;
    cin >> nume2 >> varsta2;

    if(varsta1 > varsta2)
    {
        cout << nume1 << " este mai in varsta" << endl;
    }
    else
    {
        if(varsta2 > varsta1)
            cout << nume2 << " este mai in varsta" << endl;
        else
            cout << nume1 << " si " << nume2 << " au aceai varsta";
    }


    return 0;
}
