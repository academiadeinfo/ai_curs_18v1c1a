#include <iostream>

using namespace std;

/**
Elif și Elsa au primit fiecare de la bătrânul Switch câte x bomboane.
Pe drum, punga în care Elif ținea bomboanele s-a rupt și el a pierdut a bomboane.
Elsa, văzând cât e de supărat i-a dat b bomboane din punga ei.
Scrieți un program care citește numerele x, a și b și determină câte bomboane
au la sfârșit cei doi.
*/

int main()
{
    int x, a, b;
    int elif, elsa;

    cout << "Cate bomboane au la inceput ? ";
    cin >> x;
    cout << "Cate bomboane pierde Elif ? ";
    cin >> a;
    cout << "Cate bomboane ii da Elsa ? ";
    cin >> b;

    elif = x;
    elsa = x;

    elif = elif - a; ///elif pierde a bomboane

    elsa = elsa - b; ///elsa daruieste b bomboane

    elif = elif + b; ///elif primeste inca b bomboane

    /** Soluția matematică:
        elif = x - a + b;
        elsa = x - b
    */

    cout << "Elif: " << elif << endl; /// afișăm cate bomboane are elif
    cout << "Elsa: " << elsa; /// afișam câte bomboane are elsa

    return 0;
}
