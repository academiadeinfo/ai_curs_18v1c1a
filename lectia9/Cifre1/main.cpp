#include <iostream>

using namespace std;

///Se citeșete un număr natural n
///a) câte cifre are n?
int main()
{
    int n;
    cin >> n; ///citim un numă n

    int nrCif = 0; /// contor care numără cifrele

    ///fac ceva cu fiecare cifă a lui n:
    while(n > 0) ///cât timp n mai are cifre
    {
        ///fac ceva cu ultima cifră a lui n n % 10
        nrCif++; ///nrCif  = nrCif + 1;
        n /= 10; ///n = n / 10;  elimin ultima cifră a lui n
    }

    cout << nrCif << " cifre." << endl;


    return 0;
}
