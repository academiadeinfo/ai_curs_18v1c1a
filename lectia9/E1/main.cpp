#include <iostream>

using namespace std;

int main()
{
    int n;
    int x;
    //suma și sumaImp sunt la îceput 0
    int suma = 0;
    int sumaImp = 0;
    cout << "Introduceti numarul de termeni: ";
    cin>>n;     //citesc n
    cout << "Introduceti "<<n << "numere: ";
    while(n>0) ///Execută de n ori:
    {
        cin>>x;  //  citesc x
        suma = suma +x;  //	il adaug pe x la suma
        if (x%2 != 0)  //	daca x este impar: x % 2 != 0
            sumaImp = sumaImp+x; /// sau: sumaImp += x //il adaug pe x la sumaImp;
        n = n-1; /// sau: n--;
    }
    cout << "Suma numerelor citite este "<< suma << endl;
    cout << "Suma numerelor impare este "<< sumaImp << endl;

    return 0;
}
