#include <iostream>

using namespace std;

int main()
{
    long long n;
    cin >> n;

    bool toateImpare = true; /// considerăm că toate numerele sunt impare

    while(n > 0)//parcurgem cifrele
    {
        int u = n % 10; //ultima cifră a lui n
        if( u % 2 == 0) ///dacă întâlnim o cifră pară
            toateImpare = false;
        n /= 10; // n = n / 10
    }

    if(toateImpare)///dacă toateImpare este adevărat:
        cout << "Toate cifrele sunt impare. " << endl;///afișăm mesajul corspunzător
    else
        cout << "Nu toate cifrele sunt impare."<< endl;

    return 0;
}
