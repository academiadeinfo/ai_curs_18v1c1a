#include <iostream>

using namespace std;

///Se citește un număr natural n
///Afișați suma cifrelor lui n

int main()
{
    int n;
    cin >> n;///citim n

    int suma = 0;///luăm o variabilă suma care este la îceput 0

    while(n != 0)///cât timp n mai are cifre
    {
        int u = n % 10; ///calculez ultima cifră a lui n (n % 10)
        suma += u;      /// suma = suma + u; adaug la sumă ultima cifră a lui n
        n /= 10;  ///elimin ultima cifră a lui n ( n = n / 10) (n /= 10)
    }

    cout << "Suma cifrelor: " << suma;///afișez suma cifrelor

    return 0;
}
