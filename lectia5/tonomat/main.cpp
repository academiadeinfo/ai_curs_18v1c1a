#include <iostream>

using namespace std;

int main()
{
    ///Display opțiuni:

    cout << "Salut, vrei sa inveti info ? Hai la Academie: " << endl;
    cout << "1. Primii Pasi in IT: 350 Lei" << endl;
    cout << "2. C++ Modul 1: 350 Lei" << endl;
    // ............
    cout << "5. Robotica Nivel 1: 500 Lei" << endl;
    cout << endl;

    ///Citim optiunea
    int cod, pret;
    cout << "Introduceti codul cursului: ";
    cin >> cod;

    if(cod == 1)
    {
        ///selectăm optiunea 1
        cout << "Ai ales: Primii Pasi in IT: 350 Lei." << endl;
        pret = 350;
    }
    else if(cod == 5)
    {
        ///selectăm opțiunea 2
        cout << "Ai ales: Robotica Nivel 1: 500 Lei" << endl;
        pret = 500;
    }

    ///Efectuarea plății:
    int introdus;
    cout << "Introduceti " << pret << " Lei";///afișăm cât trebuie plătit
    cin >> introdus; ///citim valoarea introdusa
    ///verificăm și afișăm dacă valoarea introdusă este suficientă
    if(introdus >= pret)
        cout << "Multumesc, Rest: " << introdus - pret;
    else
        cout << "Fonduri Insufiecte, mai incearca ;)" << endl;



    cin >> pret; ///citire care nu face nimic, pentru a nu se termina programul;
    return 0;
}
