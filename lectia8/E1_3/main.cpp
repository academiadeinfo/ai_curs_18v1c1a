#include <iostream>

using namespace std;

int main()
{
    int x; /// numărul citit
    int maxim = 0; ///maximul de până acum, la început ceva negativ


    while (x != 0)///cât timp numărul citit nu a fost 0
    {

        cin >> x;///citim noul număr
        if (x > maxim)///dacă noul număr citit este mai mare decât maximul de până acum
            maxim = x;///atunci maximul de până acum o să fie noul număr
    }
    cout << "Maximul dintre numere este: " << maxim; ///afișam maximul


    return 0;
}
