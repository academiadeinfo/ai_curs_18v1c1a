#include <iostream>

using namespace std;

int main()
{
    int x; ///numărul citit
    int suma = 0; ///suma numerelor de până acum, la început este 0

    while ( x != 0)         ///cât timp numărul citit nu a fost 0
    {
        cout << "introduceti un numar:";
        cin >> x;           ///citim următorul număr
        suma = suma + x;    ///adăugăm la sumă noul număr

    }
    cout << "suma este: " << suma<< endl;///afișăm suma
    return 0;
}
