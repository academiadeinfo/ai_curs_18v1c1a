#include <iostream>

using namespace std;

/**
    Se citește un număr natural n.
    să se afișeze de n ori textul:
    "am învățat while-ul".
*/

int main()
{
    int n; /// de câte ori trebuie să afișez
    cin >> n; /// citesc n

    ///SOLUȚIA 1:
    cout << "Solutia 1: "<< endl;
    int i = 0; /// de câte ori am afișat până acum
    while(i < n)
    {
        cout << "Am invatat while-ul" << endl; ///fac ceva

        i = i + 1; /// cresc numărul de afișări
    }
    cout << endl << endl;

    /// SOLUȚIA 2:
    cout << "Soulutia 2: " << endl;

    int k = n;  /// de câte ori mai trebuie să afișez, la îneput n

    while(k > 0) /// cât timp numărul de afișări necesare este mai mare decât 0
    {
        cout << "Am invatat while-ul" << endl; ///fac ceva
        k = k - 1; /// mai trebuie să afișez mai puțin cu 1
    }



    return 0;
}
