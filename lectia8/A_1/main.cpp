#include <iostream>

using namespace std;

/**
Scrieți un program care execută de 5 ori următoarele instrucțiuni:
Citește un număr x și afișează ultima lui cifră.
*/

int main()
{
    int n = 5; ///de câte ori mai trebuie să execut

    while(n > 0) //cât timp mai trebuie să execut de mai mult de 0 ori
    {
        ///fac ceva: Citește un număr x și afișează ultima lui cifră.

        n = n - 1; // scad numărul de ori
    }

    return 0;
}
