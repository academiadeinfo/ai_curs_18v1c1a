#include <iostream>

using namespace std;

/**
1. Se citesc numere naturale până la întâlnirea numărului 0.
   Câte numere am citit?
*/

int main()
{
    int x; /// numarul citit
    int contor = 0; /// câte numere am citit

    while(x != 0)    ///cât timp numărul citit nu a fost 0
    {
        cout << "Introduceti un numar: ";
        cin >> x; ///citim următorul număr
        contor=contor+1;///creștem contorul
    }
    cout << "Ati introdus " << contor << " numere."; ///afișăm contorul
    cout << endl;

    return 0;
}
