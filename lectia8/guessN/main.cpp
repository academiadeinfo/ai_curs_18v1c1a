#include <iostream>

using namespace std;

int main()
{
    int numar = 234; /// numărul care trebuie ghicit
    int vieti = 10; /// numărul de vieți rămase

    int ghicit; /// numărul ghicit de jucător

    while(vieti > 0 and ghicit != numar)  /// cât timp jucătorul mai are vieți ȘI nu a câștigat
    {
        cout << "Introduceti un numar: "; //îi cerem jucătorului să introducă un număr
        cin >> ghicit;  //citim numărul ghicit de jucător
        ///verificăm dacă numărul ghicit este corect (îl comparăm cu numar):
        if(ghicit < numar) // dacă este mai mic
        {
            cout << "Numarul introdus este prea mic" << endl; // afișăm că este prea mic
            vieti = vieti - 1; // scadem o viata
        }
        else if(ghicit > numar)// dacă este mai mare ->afișăm că este mai mare
        {
            cout << "Numarul introdus este prea mare" << endl;
            vieti = vieti - 1; // scadem o viata
        }
        else // dacă este egal -> afișăm că a câștigat
            cout << "BRAVO! Ai ghicit !" << endl;
    }
    if(vieti == 0)
        cout << "Ai piedut :("<<endl<<"mai mult noroc data viitoare" << endl;

    return 0;
}
