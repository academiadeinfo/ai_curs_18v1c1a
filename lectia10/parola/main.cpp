#include <iostream>

using namespace std;

///* schimbăm programul: utilizatorul va avea maxim n încercări

int main()
{
    string corect = "academia"; //parola corectă
    string parola;

    while(parola != corect)///cât timp parola introdusă de utilizator este greșită
    {
        cout << "Introduceti parola: ";///cerem parola
        cin >> parola;///citim parola introdusă
    }

    cout << "Parola corecta, poti intra." << endl;


    return 0;
}
