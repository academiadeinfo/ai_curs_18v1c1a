#include <iostream>

using namespace std;

int main()
{
    int n;
    int ziua = 1;
    int capete = 5;
    cin >> n; // datele de intrare: numărul de zile
    while(ziua < n) /// cât timp nu am ajuns la ziua curentă
    {
        capete += 6; // capete = capete + 6
        capete --;   //capete scade cu 1;
        ziua ++; // ziua = ziua + 1
    }
    cout << capete;
    return 0;
}
