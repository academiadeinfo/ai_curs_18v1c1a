#include <iostream>

using namespace std;

int main()
{
    int n;
    cout << "n = ";
    cin >> n;

    int randuri = 0;

    while(randuri < n) //cât timp nu am afișat n rânduri
    {
        ///Afișez un rând:
        int i = 0; // câte steluțe am afișat
        while(i < n)
        {
            cout << "\003 ";
            i++; // creștem i cu 1
        }
        cout << endl;

        randuri++;///cresc nr de rânduri afișate
    }


    return 0;
}
