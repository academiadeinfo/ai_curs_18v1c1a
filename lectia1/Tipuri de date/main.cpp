#include <iostream>

using namespace std;

int main()
{
    //Acesta este un comentariu
    /// Acesta este un comentariu îngroșat
    /**
        Acesta este un comentariu
        Pe mai multe
        Linii
    */


    double var = 5.7; // double - număr zecimal
    char c = 'P'; // char - caracter (între apostroafe)

    string text = "To be continued ..."; // string - text (între ghilimele)

    cout << var << " " << c << " " << text; //afișarea tuturor variabilelor

    return 0;
}
