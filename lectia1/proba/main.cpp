#include <iostream>

using namespace std;

int main()
{
    string nume = "Elif";
    int varsta = 13;
    double inaltime = 1.53;
    int an = 2016;
    double rataCrestere = 0.07;

    cout << "In " << an << " " << nume << " avea " << varsta << " ani ";
    cout << " si " << inaltime << "m. " << endl;

    ///trece un an

    an = an + 1;
    varsta = varsta + 1;
    inaltime = inaltime + rataCrestere;

    cout << "In " << an << " " << nume << " avea " << varsta << " ani ";
    cout << " si " << inaltime << "m. " << endl;

    return 0;
}
