#include <iostream>

using namespace std;

int main()
{
    int a, b; // declararea a două variabile de tipul int (numere întregi)

    a = 5; // a primește valoarea 5 (operația de atribuire)
    b = 7; // b primește valoarea 7
    cout << a + b << " "; // se va afișa valoarea 12

    a = 9; // a primește valoarea 9

    cout << a + b << " "; // se va afișa valoarea 16 (adică 9+7)

    a = a + 3; // a primește valoarea expresiei a + 3, adică 9 + 3 adică 12
    cout << a + b << endl; // se va afișa valoarea 19 (adică 12 + 7)

    return 0;
}
